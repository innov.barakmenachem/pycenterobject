from PIL import Image
import enum

"""
            Center-range
-------------------------------------
|               ######               |
|               ######               |
|               ######               |
|               ######               |
|               ######               |
|               ######               |
|               ######               |
|               ######               |
|               ######               |
-------------------------------------

"""



class Direction(enum.Enum):
   TURN_RIGHT = 1
   TURN_LEFT = 2
   DRIVE_FORWORD = 3



class Pixels:
    """represents pixles in image"""
    def __init__(self, amount):
        self.amount = amount
    def get_amount(self):
        return self.amount
    
class ImageExtractor:
    """Take image and extract data from it"""
    
    def __init__(self, url):
        self.url = url
        with Image.open(url) as img:
            self.width, self.height = img.size
        self.center = Point(self.width/2, self.height/2)    


class Point:
    """ Create a new Point, at coordinates x, y """

    def __init__(self, x=0, y=0):
        """ Create a new point at x, y """
        self.x = x
        self.y = y

    def distance_from_origin(self, point):
        """ Compute my distance from the origin """
        return ((self.x ** 2) + (self.y ** 2)) ** 0.5
    

    def distance(self, point):
        """ Compute my distance from the origin """
        return (((self.x - point.x) ** 2) + (self.y ** 2)) ** 0.5
    
    def to_string(self):
        return "({0}, {1})".format(self.x, self.y)


class centerObject:
    'class that handle the image and can infer data from it'
    def __init__(self, center_limits_range):
        self.center_limits_range = center_limits_range

    def set_frame(self, url):
        self.current_frame = ImageExtractor(url)

    def get_x_distance_from_center(self, other):
        return self.current_frame.center.x - center_limits_range.get_amount() - other.x
    
    def get_direction_to_center(self,obj_center_x, obj_center_y):
        dist_x = get_x_distance_from_center(Point(obj_center_x, obj_center_y))
        if dist_x > 0:
            return Direction.TURN_LEFT
        elif dist_x < 0:
            return Direction.TURN_RIGHT
        else:
            return Direction.DRIVE_FORWORD

brain = centerObject(10)
brain.set_frame('./OPIC-320x213.png')

